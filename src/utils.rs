use crate::components;

pub fn move_in_direction(
    transform: &mut components::Transform, 
    facing: &components::Facing,
    speed: f32,
    dt: f32
) {
    let movement_amount = speed * dt;

    transform.move_along_global(
        facing.direction_vector(),
        movement_amount
    );
}

pub fn bound_transform(
    transform: &mut components::Transform,
    min_x: f32,
    max_x: f32,
    x_margin: f32,
    min_y: f32,
    max_y: f32,
    y_margin: f32
) {
    let orig_x = transform.translation().x;
    let orig_y = transform.translation().y;

    let new_x = orig_x
        .max(min_x + x_margin)
        .min(max_x - x_margin);

    let new_y = orig_y
        .max(min_y + y_margin)
        .min(max_y - y_margin);

    transform.set_x(new_x);
    transform.set_y(new_y);
}