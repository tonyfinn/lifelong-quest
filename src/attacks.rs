mod core;
mod axe;
mod fireball;
mod gold_sword;
mod sword;

pub use self::core::AttackBuilder;
pub use axe::Axe;
pub use fireball::Fireball;
pub use gold_sword::GoldSword;
pub use sword::Sword;

use crate::{
    components,
    resources
};

use amethyst::{
    renderer::SpriteRender
};

#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub enum AttackType {
    Sword,
    Axe,
    Fireball,
    GoldSword
}

impl AttackType {
    pub fn builder(&self) -> Box<dyn AttackBuilder> {
        match self {
            AttackType::Sword => Box::new(Sword),
            AttackType::Axe => Box::new(Axe),
            AttackType::Fireball => Box::new(Fireball),
            AttackType::GoldSword => Box::new(GoldSword)
        }
    }

    pub fn cooldown(&self) -> f64 {
        self.builder().cooldown()
    }

    pub fn damage(&self) -> u8 {
        self.builder().damage()
    }

    pub fn expire_time(&self) -> f64 {
        self.builder().expire_time()
    }

    pub fn is_projectile(&self) -> bool {
        self.builder().is_projectile()
    }

    pub fn sprite(&self, asset_manager: &resources::AssetManager) -> SpriteRender {
        self.builder().sprite(asset_manager)
    }

    pub fn transform(
        &self, 
        attacker_transform: &components::Transform, 
        facing: &components::Facing
    ) -> components::Transform {
        self.builder().transform(attacker_transform, facing)
    }
}