pub struct Sword;

use amethyst::{
    core::Transform,
    renderer::SpriteRender
};

use crate::attacks::core::AttackBuilder;


pub const SWORD_COOLDOWN: f64 = 1.0;
pub const SWORD_LENGTH: f32 = 19.0;

use crate::{components,constants,resources};

impl AttackBuilder for Sword {
    fn cooldown(&self) -> f64 { SWORD_COOLDOWN }
    fn expire_time(&self) -> f64 { 0.5 }
    fn is_projectile(&self) -> bool { false }
    fn damage(&self) -> u8 { 5 }
    fn sprite(&self, asset_manager: &resources::AssetManager) -> SpriteRender {
        asset_manager.sword_sprite()
    }

    fn transform(&self, attacker_transform: &Transform, facing: &components::Facing) -> Transform {
        let mut attack_transform = attacker_transform.clone();
        let chr_x = attacker_transform.translation().x;
        let chr_y = attacker_transform.translation().y;
        match facing {
            components::Facing::Left => {
                attack_transform
                    .set_x(chr_x - (constants::CHARACTER_WIDTH / 2.0) - SWORD_LENGTH)
                    .set_rotation_euler(0.0, 0.0, std::f32::consts::PI/2.0)
            },
            components::Facing::Right => {
                attack_transform
                    .set_x(chr_x + (constants::CHARACTER_WIDTH / 2.0) + SWORD_LENGTH)
                    .set_rotation_euler(0.0, 0.0, -std::f32::consts::PI/2.0)
            },
            components::Facing::Up => {
                attack_transform
                    .set_y(chr_y + (constants::CHARACTER_HEIGHT / 2.0) + SWORD_LENGTH)
            },
            components::Facing::Down => {
                attack_transform
                    .set_y(chr_y - (constants::CHARACTER_HEIGHT / 2.0) - SWORD_LENGTH)
                    .set_rotation_euler(0.0, 0.0, std::f32::consts::PI)
            }
        };
        attack_transform.set_z(0.2);
        attack_transform
    }
}