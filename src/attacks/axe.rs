pub struct Axe;

use amethyst::{
    core::Transform,
    renderer::SpriteRender
};

use crate::attacks::core::AttackBuilder;


pub const AXE_COOLDOWN: f64 = 3.0;
pub const AXE_LENGTH: f32 = 19.0;

use crate::{components,constants,resources};

impl AttackBuilder for Axe {
    fn cooldown(&self) -> f64 { AXE_COOLDOWN }
    fn expire_time(&self) -> f64 { 0.5 }
    fn damage(&self) -> u8 { 10 }
    fn is_projectile(&self) -> bool { false }
    fn sprite(&self, asset_manager: &resources::AssetManager) -> SpriteRender {
        asset_manager.axe_sprite()
    }

    fn transform(&self, attacker_transform: &Transform, facing: &components::Facing) -> Transform {
        let mut attack_transform = attacker_transform.clone();
        let chr_x = attacker_transform.translation().x;
        let chr_y = attacker_transform.translation().y;
        match facing {
            components::Facing::Left => {
                attack_transform
                    .set_x(chr_x - (constants::CHARACTER_WIDTH / 2.0) - AXE_LENGTH)
                    .set_rotation_euler(0.0, 0.0, std::f32::consts::PI/2.0)
            },
            components::Facing::Right => {
                attack_transform
                    .set_x(chr_x + (constants::CHARACTER_WIDTH / 2.0) + AXE_LENGTH)
                    .set_rotation_euler(0.0, 0.0, -std::f32::consts::PI/2.0)
            },
            components::Facing::Up => {
                attack_transform
                    .set_y(chr_y + (constants::CHARACTER_HEIGHT / 2.0) + AXE_LENGTH)
            },
            components::Facing::Down => {
                attack_transform
                    .set_y(chr_y - (constants::CHARACTER_HEIGHT / 2.0) - AXE_LENGTH)
                    .set_rotation_euler(0.0, 0.0, std::f32::consts::PI)
            }
        };
        attack_transform.set_z(0.2);
        attack_transform
    }
}