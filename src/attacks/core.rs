use amethyst::{
    core::Transform,
    renderer::SpriteRender
};

use crate::{
    components,
    resources
};

pub trait AttackBuilder {
    fn transform(&self, attacker_transform: &Transform, facing: &components::Facing) -> Transform;
    fn sprite(&self, assets: &resources::AssetManager) -> SpriteRender;
    fn cooldown(&self) -> f64;
    fn is_projectile(&self) -> bool;
    fn expire_time(&self) -> f64;
    fn damage(&self) -> u8;
}