use amethyst::{
    prelude::*,
    core::Transform,
    core::nalgebra as amna,
    renderer::{
        Camera, Projection, Transparent
    }
};

use crate::{
    components,
    constants,
    resources::AssetManager
};

pub struct LifelongQuest;

impl LifelongQuest {
    fn setup_camera(&self, world: &mut World) {
        let mut transform = Transform::default();
        transform.set_z(1.0);
        world
            .create_entity()
            .with(Camera::from(Projection::orthographic(
                0.0,
                constants::WINDOW_WIDTH,
                0.0,
                constants::WINDOW_HEIGHT,
            )))
            .with(transform)
            .build();
    }

    fn setup_player(&self, world: &mut World, asset_manager: &AssetManager) {
        let mut player_transform = Transform::default();
        player_transform.set_xyz(50.0, 50.0, 0.1);
        world.create_entity()
            .with(player_transform)
            .with(components::Character::player())
            .with(Transparent::default())
            .with(asset_manager.mc_sprite(&components::Facing::Down, 0))
            .build();
    }

    fn setup_map(&self, world: &mut World, asset_manager: &AssetManager) {
        let mut transform = Transform::default();
        transform.set_xyz(constants::MAP_WIDTH / 2.0, constants::MAP_HEIGHT / 2.0, 0.0);
        world.create_entity()
            .with(transform)
            .with(asset_manager.map_sprite())
            .build();
    }

    fn setup_hp_meter(&self, world: &mut World, asset_manager: &AssetManager) {
        let mut transform = Transform::default();
        transform.set_xyz(constants::UI_MARGIN + 32.0, constants::UI_MARGIN + 30.0, 0.5);

        for i in 0..5 {
            world.create_entity()
                .with(components::HpMeter { index: i })
                .with(transform.clone())
                .with(asset_manager.hp_sprite(10))
                .with(Transparent)
                .build();
            transform.translate_x(65.0);
        }

        world.create_entity()
            .with(components::AttackDisplay)
            .with(transform.clone())
            .with(components::AttackType::Sword.sprite(asset_manager))
            .with(Transparent)
            .build();
    }

    fn setup_tokens(&self, world: &mut World, asset_manager: &AssetManager) {
        for token_type in components::TokenType::all().iter() {
            let mut transform = Transform::default();
            transform.set_position(token_type.location());
            transform.set_z(0.4);
            world.create_entity()
                .with(transform)
                .with(asset_manager.token_sprite(token_type.clone()))
                .with(components::Token {
                    token_type: token_type.clone(),
                    collected: false
                })
                .with(Transparent)
                .build();
        }
    }

    fn setup_knights(&self, world: &mut World, asset_manager: &AssetManager) {
        for token_type in components::TokenType::all().iter() {
            let mut transform = Transform::default();
            transform.set_position(token_type.location());
            transform.set_z(0.2);

            for i in 0..5 {
                world.create_entity()
                    .with(transform.clone())
                    .with(components::Character::knight())
                    .with(components::AiState {
                        last_direction_change_time: 0.0,
                        objective: components::AiObjective::Idle,
                        home: transform.clone()
                    })
                    .with(asset_manager.knight_sprite(&components::Facing::Down))
                    .with(Transparent)
                    .build();
            }
        }
    }

    fn setup_pads(&self, world: &mut World, asset_manager: &AssetManager) {
        let positions = vec![
            (amna::Vector3::new(700.0, 200.0, 0.05), components::AttackType::Axe),
            (amna::Vector3::new(2700.0, 1100.0, 0.05), components::AttackType::GoldSword),
            (amna::Vector3::new(1080.0, 3300.0, 0.05), components::AttackType::Axe),
            (amna::Vector3::new(3400.0, 2900.0, 0.05), components::AttackType::GoldSword)
        ];

        for (position, reward) in positions.into_iter() {
            let mut transform = Transform::default();
            transform.set_position(position);

            let mut reward_transform = transform.clone();
            reward_transform.set_z(0.08);
            reward_transform.set_scale(2.0, 2.0, 1.0);

            let reward_sprite = world.create_entity()
                .with(reward.sprite(asset_manager))
                .with(reward_transform)
                .build();

            world.create_entity()
                .with(transform)
                .with(asset_manager.pad_sprite(components::PadState::Inactive))
                .with(components::PowerUpPad::new(reward, reward_sprite))
                .build();
        }
    }
}

impl SimpleState for LifelongQuest {

    fn on_start(&mut self, data: StateData<'_, GameData<'_, '_>>) {
        let world = data.world;
        let asset_manager = AssetManager::new(world);
        self.setup_camera(world);
        self.setup_map(world, &asset_manager);
        self.setup_player(world, &asset_manager);
        self.setup_hp_meter(world, &asset_manager);
        self.setup_pads(world, &asset_manager);
        self.setup_tokens(world, &asset_manager);
        self.setup_knights(world, &asset_manager);

        world.add_resource(asset_manager);
    }
}