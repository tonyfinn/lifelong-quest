pub const CHARACTER_WIDTH: f32 = 26.0;
pub const CHARACTER_HEIGHT: f32 = 58.0;

pub const MAP_WIDTH: f32 = 4096.0;
pub const MAP_HEIGHT: f32 = 4096.0;

pub const WINDOW_WIDTH: f32 = 1280.0;
pub const WINDOW_HEIGHT: f32 = 720.0;

pub const AGGRO_RANGE: f32 = 250.0;

pub const UI_MARGIN: f32 = 20.0;