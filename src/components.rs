use amethyst::{
    ecs::prelude::*,
    core::nalgebra as amna
};

pub use amethyst::core::Transform;

use specs_derive::Component;

pub use ncollide2d::world::CollisionObjectHandle;

pub use crate::attacks::AttackType;

#[derive(Debug, PartialEq, Eq, Clone)]
pub enum Facing {
    Up,
    Down,
    Left,
    Right
}

impl Default for Facing {
    fn default() -> Self {
        Facing::Down
    }
}

impl Facing {
    pub fn direction_vector(&self) -> amna::Unit<amna::Vector3<f32>> {
        match self {
            Facing::Up => amna::Unit::new_unchecked(amna::Vector3::new(0.0, 1.0, 0.0)),
            Facing::Down => amna::Unit::new_unchecked(amna::Vector3::new(0.0, -1.0, 0.0)),
            Facing::Left => amna::Unit::new_unchecked(amna::Vector3::new(-1.0, 0.0, 0.0)),
            Facing::Right => amna::Unit::new_unchecked(amna::Vector3::new(1.0, 0.0, 0.0))
        }
    }
}

#[derive(PartialEq, Eq, Clone, Debug)]
pub enum Team {
    Player,
    Allied,
    Enemy
}

impl Default for Team {
    fn default() -> Team {
        Team::Enemy
    }
}

#[derive(Debug)]
pub enum CharacterModel {
    Knight, Player
}

pub trait HasSpriteSize {
    fn sprite_size(&self) -> (f32, f32);
}

pub trait HasTeam {
    fn team(&self) -> Team;
}

#[derive(Debug, PartialEq, Eq, Clone)]
pub enum AiObjective {
    Idle,
    Hostile,
    Returning
}

impl Default for AiObjective {
    fn default() -> AiObjective {
        AiObjective::Hostile
    }
}

#[derive(Debug, Component)]
#[storage(DenseVecStorage)]
pub struct AiState {
    pub objective: AiObjective,
    pub home: Transform,
    pub last_direction_change_time: f64
}

impl AiState {
    pub fn new(home: Transform) -> AiState {
        AiState {
            objective: AiObjective::Idle,
            last_direction_change_time: 0.0,
            home
        }
    }
}

#[derive(Debug, Component)]
#[storage(DenseVecStorage)]
pub struct Character {
    pub facing: Facing,
    pub team: Team,
    pub model: CharacterModel,
    pub powerups_collected: u8,
    pub animation_lock_time: f64,
    pub active_attack: AttackType,
    pub available_attacks: Vec<AttackType>,
    pub current_attack_idx: usize,
    pub hp: u8
}

impl Character {
    pub fn player() -> Character {
        Character {
            facing: Facing::Down,
            model: CharacterModel::Player,
            team: Team::Player,
            powerups_collected: 0,
            animation_lock_time: 0.0,
            active_attack: AttackType::Sword,
            available_attacks: vec![AttackType::Sword],
            current_attack_idx: 0,
            hp: 50
        }
    }

    pub fn knight() -> Character {
        Character {
            facing: Facing::Down,
            model: CharacterModel::Knight,
            team: Team::Enemy,
            powerups_collected: 0,
            animation_lock_time: 0.0,
            active_attack: AttackType::Sword,
            available_attacks: vec![],
            current_attack_idx: 0,
            hp: 10
        }
    }
}

impl HasSpriteSize for Character {
    fn sprite_size(&self) -> (f32, f32) {
        match (&self.model, &self.facing) {
            (CharacterModel::Player, Facing::Down) => (34.0, 58.0),
            (CharacterModel::Player, Facing::Up) => (34.0, 58.0),
            (CharacterModel::Player, Facing::Left) => (26.0, 58.0),
            (CharacterModel::Player, Facing::Right) => (26.0, 58.0),

            (CharacterModel::Knight, Facing::Down) => (30.0, 60.0),
            (CharacterModel::Knight, Facing::Up) => (30.0, 60.0),
            (CharacterModel::Knight, Facing::Left) => (18.0, 60.0),
            (CharacterModel::Knight, Facing::Right) => (18.0, 60.0),
        }
    }
}

impl HasTeam for Character {
    fn team(&self) -> Team {
        self.team.clone()
    }
}

#[derive(Component)]
#[storage(DenseVecStorage)]
pub struct Collider {
    pub handle: CollisionObjectHandle
}

#[derive(Component)]
#[storage(DenseVecStorage)]
pub struct AttackDisplay;

#[derive(Component)]
#[storage(DenseVecStorage)]
pub struct Attack {
    pub team: Team,
    pub attack_type: AttackType,
    pub expire_at: f64
}

impl HasTeam for Attack {
    fn team(&self) -> Team { self.team.clone() }
}

impl HasSpriteSize for Attack {
    fn sprite_size(&self) -> (f32, f32) {
        match self.attack_type {
            AttackType::Sword => (7.0, 19.0),
            AttackType::GoldSword => (7.0, 19.0),
            AttackType::Axe => (19.0, 19.0),
            AttackType::Fireball => (19.0, 19.0)
        }
    }
}

#[derive(Component)]
#[storage(DenseVecStorage)]
pub struct HpMeter {
    pub index: u8
}

#[derive(Default, Component)]
#[storage(NullStorage)]
pub struct WeaponDisplay;

#[derive(PartialEq, Eq, Clone, Copy)]
pub enum TokenType {
    Desert,
    Swamp,
    Rock,
    Forest,
}

impl TokenType {
    pub fn all() -> [TokenType; 4] {
        [
            TokenType::Desert, 
            TokenType::Swamp, 
            TokenType::Rock, 
            TokenType::Forest
        ]
    }

    pub fn order(&self) -> u32 {
        match self {
            TokenType::Desert => 0,
            TokenType::Swamp => 1,
            TokenType::Rock => 2,
            TokenType::Forest => 3
        }
    }

    pub fn location(&self) -> amna::Vector3<f32> {
        match self {
            TokenType::Desert => amna::Vector3::new(1700.0, 2640.0, 0.0),
            TokenType::Swamp => amna::Vector3::new(250.0, 3750.0, 0.0),
            TokenType::Rock => amna::Vector3::new(3100.0, 3350.0, 0.0),
            TokenType::Forest => amna::Vector3::new(3700.0, 1600.0, 0.0)
        }
    }
}

#[derive(Component)]
#[storage(DenseVecStorage)]
pub struct Token {
    pub token_type: TokenType,
    pub collected: bool
}

#[derive(PartialEq, Eq, Clone, Copy)]
pub enum PadState {
    Inactive,
    Approaching,
    Active,
    Used
}

#[derive(Component)]
#[storage(DenseVecStorage)]
pub struct PowerUpPad {
    pub state: PadState,
    pub reward: AttackType,
    pub reward_sprite_entity: Entity
}

impl PowerUpPad {
    pub fn new(reward: AttackType, reward_sprite_entity: Entity) -> PowerUpPad {
        PowerUpPad {
            state: PadState::Inactive,
            reward,
            reward_sprite_entity
        }
    }
}