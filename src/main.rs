extern crate amethyst;

mod attacks;
mod components;
mod constants;
mod resources;
mod setup;
mod systems;
mod utils;

use amethyst::{
    prelude::*,
    input::InputBundle,
    core::TransformBundle,
    renderer::{ALPHA, ColorMask, DisplayConfig, DrawFlat2D, Pipeline, RenderBundle, Stage},
    utils::application_root_dir,
    LoggerConfig, LogLevelFilter
};

use crate::{
    setup::LifelongQuest
};

fn main() -> amethyst::Result<()> {
    amethyst::start_logger(LoggerConfig {
        level_filter: LogLevelFilter::Warn,
        ..Default::default()
    });

    let path = format!(
        "{}/resources/display_config.ron",
        application_root_dir()
    );
    let config = DisplayConfig::load(&path);

    let pipe = Pipeline::build().with_stage(
        Stage::with_backbuffer()
            .clear_target([0.0, 0.0, 0.0, 1.0], 1.0)
            .with_pass(DrawFlat2D::new().with_transparency(
                ColorMask::all(), 
                ALPHA,
                None
            )),
    );

    let input_bundle = InputBundle::<String, String>::new()
        .with_bindings_from_file(
            format!("{}/resources/input_bindings.ron", application_root_dir())
        )?;

    let game_data =
        GameDataBuilder::default()
            .with_bundle(RenderBundle::new(pipe, Some(config))
                .with_sprite_sheet_processor()
                .with_sprite_visibility_sorting(&[])
            )?
            .with_bundle(TransformBundle::new())?
            .with_bundle(input_bundle)?
            .with(systems::RemoveDeadSystem, "remove_dead", &[])
            .with(systems::HpDisplaySystem, "hp_display", &[])
            .with(systems::AttackDisplaySystem, "attack_display", &[])
            .with(systems::CleanupExpiredAttackSystem, "cleanup_attacks", &[])
            .with_barrier()
            .with(systems::PlayerMovementSystem, "player_movement", &[])
            .with(systems::PlayerAttackSystem::default(), "player_attack", &[])
            //.with(systems::KnightSpawnSystem::default(), "knight_spawn", &[])
            .with(systems::EnemyAiSystem::default(), "enemy_ai", &[])
            .with_barrier()
            .with(systems::TokenSystem::default(), "token", &[])
            .with(systems::PadDetectSystem, "pad_detect", &[])
            .with(systems::MapScrollSystem, "map_scroll", &[])
            .with(systems::CleanupColliderSystem, "cleanup_collider", &[])
            .with(systems::MoveColliderSystem, "move_collider", &["cleanup_collider"])
            .with(systems::BuildColliderSystem, "build_collider", &["cleanup_collider", "move_collider"])
            .with_barrier()
            .with(systems::HandleCollisionSystem, "handle_collision", &["build_collider"])
            .with(systems::GameOverSystem::default(), "game_over", &["handle_collision"]);
    let mut game = Application::new("./", LifelongQuest, game_data)?;

    game.run();

    Ok(())
}
