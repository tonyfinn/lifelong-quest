use amethyst::{
    prelude::*,
    ecs::prelude::*,
    assets::{AssetStorage, Loader},
    renderer::{
        Texture, PngFormat, TextureMetadata, TextureHandle,
        SpriteSheet, SpriteSheetHandle, SpriteSheetFormat,
        SpriteRender, TgaFormat
    },
    utils::application_root_dir
};

use ncollide2d::{
    world::CollisionWorld
};

use crate::components;

pub struct AssetManager {
    pub main_character_ss: SpriteSheetHandle,
    pub map_ss: SpriteSheetHandle,
    pub pad_ss: SpriteSheetHandle,
    pub attack_ss: SpriteSheetHandle,
    pub knight_ss: SpriteSheetHandle,
    pub tokens_ss: SpriteSheetHandle,
    pub ui_ss: SpriteSheetHandle,
    pub win_ss: SpriteSheetHandle,
    pub lose_ss: SpriteSheetHandle
}

pub struct CollisionDetection {
    pub world: CollisionWorld<f32, Entity>
}

impl Default for CollisionDetection {
    fn default() -> Self {
        CollisionDetection {
            world: CollisionWorld::new(0.02)
        }
    }
}

pub enum GameOverCondition {
    Win, Lose
}

pub struct GameOverState {
    pub over: bool,
    pub reason: GameOverCondition
}

impl Default for GameOverState {
    fn default() -> GameOverState {
        GameOverState {
            over: false,
            reason: GameOverCondition::Lose
        }
    }
}

fn sprite_number_from_facing(facing: &components::Facing) -> usize {
    match facing {
        components::Facing::Up => 0,
        components::Facing::Left => 1,
        components::Facing::Right => 2,
        components::Facing::Down => 3
    }
}

fn load_texture_png(loader: &Loader, texture_storage: &AssetStorage<Texture>, path: &str) -> TextureHandle {
    loader.load(
        format!("{}/resources/textures/{}.png", application_root_dir(), path),
        PngFormat,
        TextureMetadata::srgb_scale(),
        (),
        texture_storage
    )
}

fn load_sprite(world: &World, name: &str) -> SpriteSheetHandle {
    let loader = world.read_resource::<Loader>();
    let texture_storage = world.read_resource::<AssetStorage<Texture>>();
    let sprite_storage = world.read_resource::<AssetStorage<SpriteSheet>>();

    let texture_handle = load_texture_png(&loader, &texture_storage, name);
    loader.load(
        format!("{}/resources/data/{}.ron", application_root_dir(), name),
        SpriteSheetFormat,
        texture_handle,
        (),
        &sprite_storage
    )
}

fn load_map(world: &World, name: &str) -> SpriteSheetHandle {
    let loader = world.read_resource::<Loader>();
    let texture_storage = world.read_resource::<AssetStorage<Texture>>();
    let sprite_storage = world.read_resource::<AssetStorage<SpriteSheet>>();

    let texture_handle = loader.load(
        format!("{}/resources/textures/{}.tga", application_root_dir(), name),
        TgaFormat,
        TextureMetadata::srgb_scale(),
        (),
        &texture_storage
    );
    loader.load(
        format!("{}/resources/data/{}.ron", application_root_dir(), name),
        SpriteSheetFormat,
        texture_handle,
        (),
        &sprite_storage
    )
}

impl AssetManager {
    pub fn new(world: &World) -> AssetManager {
        let main_character_ss = load_sprite(world, "mc-sprites");        
        let map_ss = load_map(world, "map-big");
        let pad_ss = load_sprite(world, "powerup-pad");
        let attack_ss = load_sprite(world, "attack-sprites");
        let tokens_ss = load_sprite(world, "tokens");
        let knight_ss = load_sprite(world, "knight-sprites");
        let ui_ss = load_sprite(world, "ui-sprites");
        let win_ss = load_sprite(world, "win");
        let lose_ss = load_sprite(world, "lose");

        AssetManager {
            main_character_ss,
            map_ss,
            pad_ss,
            attack_ss,
            tokens_ss,
            knight_ss,
            ui_ss,
            win_ss,
            lose_ss
        }
    }

    pub fn mc_sprite(&self, facing: &components::Facing, powerups_collected: usize) -> SpriteRender {
        let age_offset = powerups_collected.min(3) * 4;

        SpriteRender {
            sprite_sheet: self.main_character_ss.clone(),
            sprite_number: age_offset + sprite_number_from_facing(facing)
        }
    }    
    
    pub fn knight_sprite(&self, facing: &components::Facing) -> SpriteRender {
        SpriteRender {
            sprite_sheet: self.knight_ss.clone(),
            sprite_number: sprite_number_from_facing(facing)
        }
    }
    
    pub fn map_sprite(&self) -> SpriteRender {
        SpriteRender {
            sprite_sheet: self.map_ss.clone(),
            sprite_number: 0
        }
    }
    
    pub fn sword_sprite(&self) -> SpriteRender {
        SpriteRender {
            sprite_sheet: self.attack_ss.clone(),
            sprite_number: 0
        }
    }
    
    pub fn fireball_sprite(&self) -> SpriteRender {
        SpriteRender {
            sprite_sheet: self.attack_ss.clone(),
            sprite_number: 1
        }
    }
    
    pub fn axe_sprite(&self) -> SpriteRender {
        SpriteRender {
            sprite_sheet: self.attack_ss.clone(),
            sprite_number: 2
        }
    }
    
    pub fn gold_sword_sprite(&self) -> SpriteRender {
        SpriteRender {
            sprite_sheet: self.attack_ss.clone(),
            sprite_number: 3
        }
    }

    pub fn hp_sprite(&self, segment_hp: u8) -> SpriteRender {
        SpriteRender {
            sprite_sheet: self.ui_ss.clone(),
            sprite_number: (segment_hp - 1) as usize
        }
    }

    pub fn token_sprite(&self, token_type: components::TokenType) -> SpriteRender {
        let sprite_number = match token_type {
            components::TokenType::Desert => 0,
            components::TokenType::Forest => 1,
            components::TokenType::Rock => 2,
            components::TokenType::Swamp => 3
        };

        SpriteRender {
            sprite_sheet: self.tokens_ss.clone(),
            sprite_number
        }
    }

    pub fn sold_hp_sprite(&self, segment_hp: u8) -> SpriteRender {
        SpriteRender {
            sprite_sheet: self.ui_ss.clone(),
            sprite_number: ((10 + segment_hp) - 1) as usize
        }
    }

    pub fn win_sprite(&self) -> SpriteRender {
        SpriteRender {
            sprite_sheet: self.win_ss.clone(),
            sprite_number: 0
        }
    }

    pub fn lose_sprite(&self) -> SpriteRender {
        SpriteRender {
            sprite_sheet: self.lose_ss.clone(),
            sprite_number: 0
        }
    }


    pub fn pad_sprite(&self, pad_state: components::PadState) -> SpriteRender {
        let sprite_number = match pad_state {
            components::PadState::Inactive => 0,
            components::PadState::Approaching => 1,
            components::PadState::Active => 2,
            components::PadState::Used => 3
        };

        SpriteRender {
            sprite_sheet: self.pad_ss.clone(),
            sprite_number
        }
    }
}