use amethyst::{
    ecs::prelude::*,
    renderer::{Camera,SpriteRender}
};

use crate::{
    components,
    constants,
    resources
};

pub struct AttackDisplaySystem;

impl<'s> System<'s> for AttackDisplaySystem {
    type SystemData = (
        WriteStorage<'s, SpriteRender>,
        WriteStorage<'s, components::Transform>,
        ReadStorage<'s, components::AttackDisplay>,
        ReadStorage<'s, components::Character>,
        ReadStorage<'s, Camera>,
        Entities<'s>,
        ReadExpect<'s, resources::AssetManager>
    );

    fn run(&mut self, (mut sprites, mut transforms, attack_displays, characters, cameras, entities, assets): Self::SystemData) {
        let player_weapon = (&characters).join()
            .find(|chr| chr.team == components::Team::Player)
            .unwrap()
            .active_attack;

        let (camera_x, camera_y) = (&cameras, &transforms).join()
            .map(|(_, transform)| (transform.translation().x, transform.translation().y))
            .nth(0)
            .unwrap();

        for (_, mut transform, entity) in (&attack_displays, &mut transforms, &entities).join() {
            let x_coord = constants::UI_MARGIN 
                + 32.0
                + camera_x
                + (5.0 * 65.0);
            let y_coord = constants::UI_MARGIN
                + 32.0
                + camera_y;
            transform.set_x(x_coord);
            transform.set_y(y_coord);
            transform.set_scale(2.0, 2.0, 1.0);
            sprites.insert(entity, player_weapon.sprite(&assets)).unwrap();
        }
    }
}