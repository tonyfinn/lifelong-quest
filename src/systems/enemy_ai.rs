use amethyst::{
    core::Time,
    core::nalgebra as amna,
    ecs::prelude::*,
    renderer::SpriteRender
};

use crate::{
    components,
    components::HasSpriteSize,
    constants,
    resources,
    utils
};

use rand::SeedableRng;
use rand_xorshift::XorShiftRng;
use rand::seq::SliceRandom;

pub struct EnemyAiSystem {
    rng: XorShiftRng
}

pub const KNIGHT_MOVEMENT_SPEED: f32 = 90.0;
pub const MAX_IDLE_ROAM: f32 = 100.0;
impl Default for EnemyAiSystem {
    fn default() -> EnemyAiSystem {
        EnemyAiSystem {
            rng: XorShiftRng::seed_from_u64(4)
        }
    }
}

impl EnemyAiSystem {

    fn move_to_target(
        &mut self,
        entity: Entity,
        player_transform: &components::Transform,
        ai_transform: &mut components::Transform,
        sprites: &mut WriteStorage<SpriteRender>,
        time: &Time,
        assets: &resources::AssetManager
    ) {

        let distance_vector = player_transform.translation() - ai_transform.translation();

        let unit_vector = distance_vector / distance_vector.norm();

        let speed = time.delta_seconds() * KNIGHT_MOVEMENT_SPEED;

        let new_translation = ai_transform.translation() + (unit_vector * speed);

        (*ai_transform.translation_mut()) = new_translation;
    }

    fn idle_routine(
        &mut self,
        entity: Entity,
        ai_state: &mut components::AiState, 
        transform: &mut components::Transform,
        sprites: &mut WriteStorage<SpriteRender>,
        character: &mut components::Character, 
        time: &Time,
        assets: &resources::AssetManager
    ) {
        let current_time = time.absolute_time_seconds();

        if current_time - ai_state.last_direction_change_time > 1.0 {

            // let (hx, hy) = (ai_state.home.translation().x, ai_state.home.translation().y);
            // let (tx, ty) = (transform.translation().x, transform.translation().y);
            let (dx, dy) = (
                transform.translation().x - ai_state.home.translation().x,
                transform.translation().y - ai_state.home.translation().y
            );

            let mut valid_directions: Vec<components::Facing> = Vec::new();

            if dx < MAX_IDLE_ROAM {
                valid_directions.push(components::Facing::Right)
            }
            if dx > -MAX_IDLE_ROAM {
                valid_directions.push(components::Facing::Left)
            }
            if dy < MAX_IDLE_ROAM {
                valid_directions.push(components::Facing::Up)
            }
            if dy > -MAX_IDLE_ROAM {
                valid_directions.push(components::Facing::Down)
            }

            let new_facing = valid_directions.choose(&mut self.rng).unwrap();
            character.facing = new_facing.clone();

            sprites.insert(entity, assets.knight_sprite(new_facing)).unwrap();
            ai_state.last_direction_change_time = current_time;
        }

        utils::bound_transform(
            transform,
            /* x */ 0.0, constants::MAP_WIDTH, character.sprite_size().0 / 2.0, 
            /* y */ 0.0, constants::MAP_HEIGHT, character.sprite_size().1 / 2.0
        );

        utils::move_in_direction(
            transform, &character.facing, KNIGHT_MOVEMENT_SPEED, time.delta_seconds()
        )
    }
}

impl<'s> System<'s> for EnemyAiSystem {
    type SystemData = (
        WriteStorage<'s, components::AiState>,
        WriteStorage<'s, components::Transform>,
        WriteStorage<'s, SpriteRender>,
        WriteStorage<'s, components::Character>,
        Read<'s, Time>,
        ReadExpect<'s, resources::AssetManager>,
        Entities<'s>
    );

    fn run(&mut self, (mut ai_states, mut transforms, mut sprites, mut characters, time, assets, entities): Self::SystemData) {

        let player_transform = (&characters, &transforms).join()
            .find(|(chr, _)| chr.team == components::Team::Player)
            .unwrap()
            .1
            .clone();

        for (mut ai_state, mut transform, mut character, entity) in (
            &mut ai_states, &mut transforms, &mut characters, &entities
        ).join() {
            if ai_state.objective == components::AiObjective::Idle {
                if (player_transform.translation() - transform.translation()).norm() < constants::AGGRO_RANGE
                    && (ai_state.home.translation() - transform.translation()).norm() < constants::AGGRO_RANGE * 1.2 {
                    ai_state.objective = components::AiObjective::Hostile
                } else {
                    self.idle_routine(
                        entity,
                        &mut ai_state, 
                        &mut transform,
                        &mut sprites,
                        &mut character,
                        &time,
                        &assets
                    )
                }
            } else if ai_state.objective == components::AiObjective::Hostile {
                if (transform.translation() - ai_state.home.translation()).norm() > (constants::AGGRO_RANGE * 2.0) {
                    println!("No longer hostile");
                    ai_state.objective = components::AiObjective::Returning
                } else {
                    self.move_to_target(entity, &player_transform, &mut transform, &mut sprites, &time, &assets);
                }
                // self.attack_player();
            } else if ai_state.objective == components::AiObjective::Returning {
                let distance_home = (transform.translation() - ai_state.home.translation()).norm();

                if distance_home > 50.0 {
                    self.move_to_target(entity, &ai_state.home, &mut transform, &mut sprites, &time, &assets);
                } else {
                    ai_state.objective = components::AiObjective::Idle
                }
            }
        }
    }
}