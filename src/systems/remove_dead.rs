use amethyst::{
    ecs::prelude::*
};

use crate::components;

pub struct RemoveDeadSystem;

impl<'s> System<'s> for RemoveDeadSystem {
    type SystemData = (
        ReadStorage<'s, components::Character>,
        Entities<'s>
    );

    fn run(&mut self, (characters, entities): Self::SystemData) {
        for (character, entity) in (&characters, &entities).join() {
            if character.team != components::Team::Player && character.hp <= 0 {
                entities.delete(entity).unwrap();
            }
        }
    }
}