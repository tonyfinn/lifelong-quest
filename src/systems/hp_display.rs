use amethyst::{
    ecs::prelude::*,
    renderer::{Camera,SpriteRender}
};

use crate::{
    components,
    constants,
    resources
};

pub struct HpDisplaySystem;

impl<'s> System<'s> for HpDisplaySystem {
    type SystemData = (
        WriteStorage<'s, SpriteRender>,
        WriteStorage<'s, components::Transform>,
        ReadStorage<'s, components::HpMeter>,
        ReadStorage<'s, components::Character>,
        ReadStorage<'s, Camera>,
        Entities<'s>,
        ReadExpect<'s, resources::AssetManager>
    );

    fn run(&mut self, (mut sprites, mut transforms, meters, characters, cameras, entities, assets): Self::SystemData) {
        let player_hp = (&characters).join()
            .find(|chr| chr.team == components::Team::Player)
            .unwrap()
            .hp;

        let (camera_x, camera_y) = (&cameras, &transforms).join()
            .map(|(_, transform)| (transform.translation().x, transform.translation().y))
            .nth(0)
            .unwrap();

        for (transform, meter, entity) in (&mut transforms, &meters, &entities).join() {
            if player_hp <= meter.index * 10 {
                sprites.remove(entity);
            } else {
                let meter_hp = (player_hp - (meter.index * 10)).min(10);
                let x_coord = constants::UI_MARGIN 
                    + 32.0
                    + camera_x
                    + (meter.index as f32 * 65.0)
                    - ((10 - meter_hp) as f32 * 3.0);
                let y_coord = constants::UI_MARGIN
                    + 32.0
                    + camera_y;
                transform.set_x(x_coord);
                transform.set_y(y_coord);
                sprites.insert(entity, assets.hp_sprite(meter_hp)).unwrap();
            }
        }
    }
}