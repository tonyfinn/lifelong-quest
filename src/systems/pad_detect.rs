use amethyst::{
    ecs::prelude::*,
    renderer::SpriteRender
};

use crate::{
    components,
    resources
};

pub struct PadDetectSystem;

impl<'s> System<'s> for PadDetectSystem {
    type SystemData = (
        WriteStorage<'s, components::Character>,
        WriteStorage<'s, components::PowerUpPad>,
        WriteStorage<'s, SpriteRender>,
        Entities<'s>,
        ReadStorage<'s, components::Transform>,
        ReadExpect<'s, resources::AssetManager>
    );

    fn run(&mut self, (mut characters, mut powerups, mut sprites, entities, transforms, assets): Self::SystemData) {
        let (mut player, player_transform) = (&mut characters, &transforms).join()
            .find(|(chr, _)| chr.team == components::Team::Player)
            .unwrap();

        for (mut pad, transform, entity) in (&mut powerups, &transforms, &entities).join() {
            if pad.state == components::PadState::Used {
                continue;
            }
            let distance = player_transform.translation() - transform.translation();
            let max_distance = distance.x.abs().max(distance.y.abs());
            if max_distance <= 21.0 && pad.state == components::PadState::Approaching {
                pad.state = components::PadState::Active;
                entities.delete(pad.reward_sprite_entity);
                player.powerups_collected += 1;
                player.hp = player.hp.min(50 - (player.powerups_collected * 10));
                if !player.available_attacks.contains(&pad.reward) {
                    player.available_attacks.push(pad.reward);
                }
                println!("Activating pad from {}", distance);
            } else if max_distance <= 64.0 && pad.state == components::PadState::Inactive {
                pad.state = components::PadState::Approaching;
                println!("Approaching pad from {}", distance);
            } else if pad.state == components::PadState::Active && max_distance > 64.0 {
                pad.state = components::PadState::Used;
            } else if pad.state == components::PadState::Approaching && max_distance > 64.0 {
                pad.state = components::PadState::Inactive
            }

            sprites.insert(entity, assets.pad_sprite(pad.state)).unwrap();
        }
    }
}