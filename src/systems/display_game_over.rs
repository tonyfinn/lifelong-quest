use amethyst::{
    ecs::prelude::*,
    renderer::{Camera,SpriteRender, Transparent}
};

use crate::{
    components,
    constants,
    resources
};

#[derive(Default)]
pub struct GameOverSystem {
    ended: bool
}

impl<'s> System<'s> for GameOverSystem {
    type SystemData = (
        WriteStorage<'s, SpriteRender>,
        WriteStorage<'s, components::Transform>,
        WriteStorage<'s, Transparent>,
        ReadStorage<'s, Camera>,
        Read<'s, resources::GameOverState>,
        ReadExpect<'s, resources::AssetManager>,
        Entities<'s>
    );

    fn run(&mut self, (mut sprites, mut transforms, mut transparents, cameras, game_over, assets, entities): Self::SystemData) {
        let (camera_x, camera_y) = (&cameras, &transforms).join()
            .map(|(_, transform)| (transform.translation().x, transform.translation().y))
            .nth(0)
            .unwrap();

        if !self.ended {
            if game_over.over {
                let sprite = match game_over.reason {
                    resources::GameOverCondition::Lose => assets.lose_sprite(),
                    resources::GameOverCondition::Win => assets.win_sprite()
                };
                self.ended = true;
                let mut transform = components::Transform::default();
                transform.set_xyz(
                    camera_x + constants::WINDOW_WIDTH / 2.0, 
                    camera_y + constants::WINDOW_HEIGHT / 2.0, 
                0.8);
                entities.build_entity()
                    .with(sprite, &mut sprites)
                    .with(transform, &mut transforms)
                    .with(Transparent, &mut transparents)
                    .build();
            }
        }
    }
}