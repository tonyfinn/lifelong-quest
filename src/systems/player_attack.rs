use amethyst::{
    core::{Transform, Time},
    ecs::prelude::*,
    input::InputHandler,
    renderer::{SpriteRender, Transparent}
};

use crate::{attacks,components,resources};


pub struct PlayerAttackSystem {
    last_attack_time: f64,
    last_change_time: f64,
    active_sword_entity: Option<Entity>
}

impl Default for PlayerAttackSystem {
    fn default() -> PlayerAttackSystem {
        PlayerAttackSystem {
            last_attack_time: 0.0,
            last_change_time: 0.0,
            active_sword_entity: None
        }
    }
}

impl PlayerAttackSystem {
    fn find_character_transform<'t, 'c>(
        &self,
        transforms: &'t WriteStorage<components::Transform>, 
        characters: &'c mut WriteStorage<components::Character>
    ) -> (&'c mut components::Character, &'t components::Transform) {
        (characters, transforms).join()
            .find(|(character, _)| character.team == components::Team::Player)
            .expect("No player with transform")
    }
}

impl<'s> System<'s> for PlayerAttackSystem {
    type SystemData = (
        WriteStorage<'s, Transform>,
        WriteStorage<'s, SpriteRender>,
        WriteStorage<'s, Transparent>,
        WriteStorage<'s, components::Attack>,
        WriteStorage<'s, components::Character>,
        Read<'s, InputHandler<String, String>>,
        ReadExpect<'s, resources::AssetManager>,
        Read<'s, Time>,
        Entities<'s>
    );

    fn run(&mut self, (mut transforms, mut sprites, mut trans, mut attacks, mut characters, input, assets, time, entities): Self::SystemData) {
        let current_time = time.absolute_time_seconds();

        let time_since_last_attack = current_time - self.last_attack_time;

        let (character, ptransform) = self.find_character_transform(&transforms, &mut characters);
        let active_attack_type = character.active_attack;
        let new_transform = active_attack_type.transform(&ptransform, &character.facing);

        if input.action_is_down("sword_attack").unwrap() && time_since_last_attack > active_attack_type.cooldown() {
            println!("Attacking");
            character.animation_lock_time = current_time + active_attack_type.expire_time();
            self.active_sword_entity = Some(entities.build_entity()
                .with(active_attack_type.sprite(&assets), &mut sprites)
                .with(components::Attack { 
                    team: components::Team::Player, 
                    attack_type: active_attack_type,
                    expire_at: current_time + active_attack_type.expire_time()
                }, &mut attacks)
                .with(new_transform, &mut transforms)
                .with(Transparent, &mut trans)
                .build());
            self.last_attack_time = current_time;
        }

        if input.action_is_down("next_weapon").unwrap() && current_time - self.last_change_time > 2.0 {
            let next_attack_idx = (character.current_attack_idx + 1) % character.available_attacks.len();
            character.active_attack = character.available_attacks.get(next_attack_idx).unwrap().clone();
            character.current_attack_idx = next_attack_idx;
            self.last_change_time = current_time;
        }
    }
}