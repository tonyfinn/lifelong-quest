use amethyst::{
    core::{Transform,Time},
    ecs::prelude::*,
    input,
    renderer::SpriteRender
};

use crate::{
    constants,
    components,
    components::HasSpriteSize,
    resources,
    utils
};

pub const CHARACTER_MOVEMENT_SPEED: f32 = 120.0;

#[derive(Default)]
pub struct PlayerMovementSystem;

impl<'s> System<'s> for PlayerMovementSystem {
    type SystemData = (
        WriteStorage<'s, Transform>,
        WriteStorage<'s, SpriteRender>,
        WriteStorage<'s, components::Character>,
        Entities<'s>,
        ReadExpect<'s, resources::AssetManager>,
        Read<'s, resources::GameOverState>,
        Read<'s, input::InputHandler<String, String>>,
        Read<'s, Time>,
    );
    
    fn run(&mut self, (mut transforms, mut sprites, mut characters, entities, ss_manager, game_over, input, time): Self::SystemData) {
        let current_time = time.absolute_time_seconds();

        let mut transforms_to_update: Vec<(Entity, Transform)> = Vec::new();

        for (mut transform, mut character, entity) in (&mut transforms, &mut characters, &entities).join() {
            if game_over.over {
                continue;
            }
            if character.team != components::Team::Player {
                continue;
            }

            if current_time < character.animation_lock_time {
                continue;
            }

            let mut directions_to_move: Vec<components::Facing> = Vec::new();
            
            if input.action_is_down("left").unwrap() {
                character.facing = components::Facing::Left;
                directions_to_move.push(components::Facing::Left);
            }
            
            if input.action_is_down("right").unwrap() {
                character.facing = components::Facing::Right;
                directions_to_move.push(components::Facing::Right);
            }

            if input.action_is_down("up").unwrap() {
                character.facing = components::Facing::Up;
                directions_to_move.push(components::Facing::Up);
            }
            
            if input.action_is_down("down").unwrap() {
                character.facing = components::Facing::Down;
                directions_to_move.push(components::Facing::Down);
            }
        

            let mut new_transform = transform.clone();

            for facing in directions_to_move {
                utils::move_in_direction(
                    &mut new_transform,
                    &facing,
                    CHARACTER_MOVEMENT_SPEED,
                    time.delta_seconds()
                );
            }

            utils::bound_transform(
                &mut new_transform,
                /* x */ 0.0, constants::MAP_WIDTH, character.sprite_size().0 / 2.0, 
                /* y */ 0.0, constants::MAP_HEIGHT, character.sprite_size().1 / 2.0
            );

            transforms_to_update.push((entity.clone(), new_transform));

            sprites.insert(entity, ss_manager.mc_sprite(&character.facing, character.powerups_collected as usize)).unwrap();
        }

        for (entity, transform) in transforms_to_update {
            transforms.insert(entity, transform).unwrap();
        }
    }
}