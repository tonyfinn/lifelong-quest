use amethyst::{
    ecs::prelude::*,
    core::nalgebra as amna,
    renderer::Camera
};

use crate::{
    constants,
    components,
    resources
};

pub struct TokenSystem {
    collected: Vec<components::TokenType>
}

impl Default for TokenSystem {
    fn default() -> TokenSystem {
        TokenSystem {
            collected: Vec::new()
        }
    }
}

impl<'s> System<'s> for TokenSystem {
    type SystemData = (
        WriteStorage<'s, components::Transform>,
        WriteStorage<'s, components::Token>,
        ReadStorage<'s, components::Character>,
        ReadStorage<'s, Camera>,
        Write<'s, resources::GameOverState>,
        Entities<'s>
    );

    fn run(&mut self, (mut transforms, mut tokens, characters, cameras, mut game_over, entities): Self::SystemData) {
        let player_transform = (&characters, &transforms).join()
            .find(|(chr, _)| chr.team == components::Team::Player)
            .map(|(_, transform)| transform.translation().clone())
            .unwrap();

        let camera_transform = (&cameras, &transforms).join()
            .map(|(_, transform)| transform.translation().clone())
            .nth(0)
            .unwrap();

        for (mut token, transform) in (&mut tokens, &transforms).join() {
            if (player_transform - transform.translation()).norm() < 20.0 {
                if !token.collected {
                    token.collected = true;
                    self.collected.push(token.token_type);
                    if self.collected.len() == 4 {
                        game_over.over = true;
                        game_over.reason = resources::GameOverCondition::Win;
                    }
                }
            }
        }

        for (token, entity) in (&tokens, &entities).join() {
            if token.collected {
                let order = token.token_type.order();
                let offset = constants::UI_MARGIN + 32.0 + (order as f32 * 64.0);

                let mut new_transform = components::Transform::default();

                new_transform.set_x(camera_transform.x + (constants::WINDOW_WIDTH - offset));
                new_transform.set_y(camera_transform.y + constants::UI_MARGIN + 32.0);

                transforms.insert(entity, new_transform).unwrap();
            }
        }
    }
}