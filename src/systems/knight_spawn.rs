use amethyst::{
    core::{Transform, Time},
    ecs::prelude::*,
    renderer::{SpriteRender, Transparent}
};

use crate::{components,resources};

pub const SPAWN_DELAY: f64 = 10.0;

#[derive(Default)]
pub struct KnightSpawnSystem {
    last_spawn_time: f64
}

impl<'s> System<'s> for KnightSpawnSystem {
    type SystemData = (
        WriteStorage<'s, components::Transform>,
        WriteStorage<'s, components::Character>,
        WriteStorage<'s, components::AiState>,
        WriteStorage<'s, Transparent>,
        WriteStorage<'s, SpriteRender>,
        ReadExpect<'s, resources::AssetManager>,
        Read<'s, Time>,
        Entities<'s>
    );

    fn run(&mut self, (mut transforms, mut enemies, mut ai_states, mut transparents, mut sprites, asset_man, time, entities): Self::SystemData) {
        let current_time = time.absolute_time_seconds();
        let time_since_last_spawn = current_time - self.last_spawn_time;

        if time_since_last_spawn > SPAWN_DELAY {
            println!("Spawning a knight");
            let mut transform = Transform::default();
            transform.set_xyz(100.0, 100.0, 0.1);
            entities.build_entity()
                .with(transform.clone(), &mut transforms)
                .with(components::Character::knight(), &mut enemies)
                .with(components::AiState {
                    last_direction_change_time: 0.0,
                    objective: components::AiObjective::Idle,
                    home: transform.clone()
                }, &mut ai_states)
                .with(Transparent, &mut transparents)
                .with(asset_man.knight_sprite(&components::Facing::Down), &mut sprites)
                .build();
            self.last_spawn_time = current_time;
        }
    }
}