use amethyst::{
    renderer::Camera,
    ecs::prelude::*,
};

use crate::{components,constants};

pub struct MapScrollSystem;

impl<'s> System<'s> for MapScrollSystem {
    type SystemData = (
        WriteStorage<'s, components::Transform>,
        ReadStorage<'s, Camera>,
        ReadStorage<'s, components::Character>
    );

    fn run(&mut self, (mut transforms, cameras, characters): Self::SystemData) {
        // Find character position
        let character_pos = {
            (&transforms, &characters).join()
                .find(|(_, character)| character.team == components::Team::Player)
                .map(|(transform, _)| transform.translation().clone())
                .expect("No character found")
        };

        // Find camera's transform
        let camera_transform = (&mut transforms, &cameras).join()
            .nth(0)
            .map(|(transform, _)| transform)
            .expect("No camera found");

        let new_camera_x = (character_pos.x - (constants::WINDOW_WIDTH / 2.0))
            .max(0.0)
            .min(constants::MAP_WIDTH - constants::WINDOW_WIDTH);
        
        let new_camera_y = (character_pos.y - (constants::WINDOW_HEIGHT / 2.0))
            .max(0.0)
            .min(constants::MAP_HEIGHT - constants::WINDOW_HEIGHT);

        camera_transform.set_x(new_camera_x);
        camera_transform.set_y(new_camera_y);

    }
}