use amethyst::{
    core::Time,
    ecs::prelude::*
};

use crate::components;

pub struct CleanupExpiredAttackSystem;

impl<'s> System<'s> for CleanupExpiredAttackSystem {
    type SystemData = (
        ReadStorage<'s, components::Attack>,
        Read<'s, Time>,
        Entities<'s>
    );

    fn run(&mut self, (attacks, time, entities): Self::SystemData) {
        let current_time = time.absolute_time_seconds();
        for (attack, entity) in (&attacks, &entities).join() {
            if current_time > attack.expire_at {
                entities.delete(entity).unwrap();
            }
        }
    }
}