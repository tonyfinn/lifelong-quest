use amethyst::{
    ecs::prelude::*
};

use crate::{
    components,
    resources
};

use ncollide2d::{
    query::Proximity,
    world::{
        CollisionObject
    }
};

pub enum CollidableType {
    Character(components::Team),
    Attack,
    Invalid
}

pub struct HandleCollisionSystem;

impl HandleCollisionSystem {
    fn handle_character_character_collision(
        &self,
        characters: &mut WriteStorage<components::Character>,
        game_over_state: &mut resources::GameOverState,
        first: Entity,
        second: Entity
    ) {
        {
            let first = characters.get(first).unwrap();
            let second = characters.get(second).unwrap();
            println!("character {:?} collides with character {:?}", first, second);
        }
        {
            let mut first = characters.get_mut(first).unwrap();
            if first.team == components::Team::Player {
                if first.hp >= 3 {
                    first.hp -= 3
                } else if !game_over_state.over {
                    game_over_state.over = true;
                    game_over_state.reason = resources::GameOverCondition::Lose;
                }
                println!("Player took 3 damage from collision");
            }
        }
        {
            let mut second = characters.get_mut(second).unwrap();
            if second.team == components::Team::Player {                
                if second.hp >= 3 {
                    second.hp -= 3
                } else if !game_over_state.over {
                    game_over_state.over = true;
                    game_over_state.reason = resources::GameOverCondition::Lose;
                }
                println!("Player took 3 damage from collision");
            }
        }
    }

    fn handle_character_attack_collision(
        &self,
        character: &mut components::Character,
        attack: &mut components::Attack
    ) {
        if character.team != components::Team::Player {
            let damage = attack.attack_type.damage();
            if character.hp >= damage {
                character.hp -= attack.attack_type.damage();
            } else {
                character.hp = 0;
            }
        }
    }

    fn handle_attack_attack_collision(
        &self,
        attacks: &mut WriteStorage<components::Attack>,
        first: Entity,
        second: Entity
    ) {
        // let mut first = attacks.get_mut(first).unwrap();
        // let mut second = attacks.get_mut(second).unwrap();
        // TODO: Handle when projectiles added
    }

    fn find_collision_type(
        &self, 
        entity: Entity,
        characters: &WriteStorage<components::Character>,
        attacks: &WriteStorage<components::Attack>
    ) -> CollidableType {
        characters
            .get(entity.clone())
            .map_or_else(
                || attacks
                    .get(entity.clone())
                    .map_or(CollidableType::Invalid, |_| CollidableType::Attack),
                |chr| CollidableType::Character(chr.team.clone())
            )
    }

    fn handle_collision(
        &self, 
        first: &CollisionObject<f32, Entity>, 
        second: &CollisionObject<f32, Entity>,
        game_over_state: &mut resources::GameOverState,
        characters: &mut WriteStorage<components::Character>,
        attacks: &mut WriteStorage<components::Attack>
    ) {
        let first_component = self.find_collision_type(first.data().clone(), characters, attacks);
        let second_component = self.find_collision_type(second.data().clone(), characters, attacks);

        match (first_component, second_component) {
            (CollidableType::Invalid, _) => println!("Unknown first collider"),
            (_, CollidableType::Invalid) => println!("Unknown second collider"),
            (CollidableType::Character(_), CollidableType::Character(_)) => self.handle_character_character_collision(
                characters, game_over_state, first.data().clone(), second.data().clone()
            ),
            (CollidableType::Character(_), CollidableType::Attack) => self.handle_character_attack_collision(
                characters.get_mut(first.data().clone()).unwrap(), 
                attacks.get_mut(second.data().clone()).unwrap(), 
            ),
            (CollidableType::Attack, CollidableType::Character(_)) => self.handle_character_attack_collision(
                characters.get_mut(second.data().clone()).unwrap(), 
                attacks.get_mut(first.data().clone()).unwrap(), 
            ),
            (CollidableType::Attack, CollidableType::Attack) => self.handle_attack_attack_collision(
                attacks,
                first.data().clone(),
                second.data().clone()
            ),
        }
    }
}

impl<'s> System<'s> for HandleCollisionSystem {
    type SystemData = (
        WriteStorage<'s, components::Character>,
        WriteStorage<'s, components::Attack>,
        WriteStorage<'s, components::AiState>,
        Write<'s, resources::CollisionDetection>,
        Write<'s, resources::GameOverState>,
        Entities<'s>
    );

    fn run(&mut self, (mut characters, mut attacks, mut ai_states, mut collision_system, mut game_over_state, entities): Self::SystemData) {
        let cworld = &mut collision_system.world;
        cworld.update();
        for event in cworld.proximity_events() {
            let first_obj = cworld.collision_object(event.collider1).unwrap();
            let second_obj = cworld.collision_object(event.collider2).unwrap();

            let first_id = first_obj.data().id();
            let second_id = second_obj.data().id();

            if event.new_status == Proximity::Intersecting {
                self.handle_collision(first_obj, second_obj, &mut game_over_state, &mut characters, &mut attacks);
                println!("{} collided with {}", first_id, second_id);
            } else if event.new_status == Proximity::WithinMargin {
                if event.prev_status == Proximity::Intersecting {
                    println!("{} stopped colliding with {}", first_id, second_id);
                } else if event.prev_status == Proximity::Disjoint {
                    println!("{} approached {}", first_id, second_id);
                }
            } else {
                println!("{} left {}", first_id, second_id);
            }
        }
    }
}