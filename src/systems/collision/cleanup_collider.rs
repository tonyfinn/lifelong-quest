use amethyst::{
    ecs::prelude::*,
};

use crate::{
    resources
};

use ncollide2d::world::{CollisionObjectHandle};

pub struct CleanupColliderSystem;

impl<'s> System<'s> for CleanupColliderSystem {
    type SystemData = (
        Write<'s, resources::CollisionDetection>,
        Entities<'s>
    );

    fn run(&mut self, (mut collision_system, entities): Self::SystemData) {
        let mut handles_to_remove = Vec::<CollisionObjectHandle>::new();
        for collision_object in collision_system.world.collision_objects() {
            let entity = collision_object.data().clone();
            let entity_id = entity.id();
            if !entities.is_alive(entity) {
                println!("Removing collider for entity {}", entity_id);
                handles_to_remove.push(collision_object.handle());
            }
        }

        collision_system.world.remove(&handles_to_remove);
    }
}