use amethyst::{
    ecs::prelude::*
};

use crate::{
    components::{Collider,Transform},
    resources,
    systems::collision::build_collider::build_pos_for_transform
};

pub struct MoveColliderSystem;

impl<'s> System<'s> for MoveColliderSystem {
    type SystemData = (
        ReadStorage<'s, Collider>,
        ReadStorage<'s, Transform>,
        Write<'s, resources::CollisionDetection>,
    );

    fn run(&mut self, (colliders, transforms, mut collision_detection): Self::SystemData) {
        for (collider, transform) in (&colliders, &transforms).join() {
            collision_detection.world.set_position(
                collider.handle,
                build_pos_for_transform(transform)
            );
        }
    }    
}