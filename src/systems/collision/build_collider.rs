use amethyst::{
    ecs::prelude::*
};

use nalgebra as ncna;

use ncollide2d::{
    shape,
    world as ncworld
};

pub const PIXELS_PER_COLLIDER_UNIT: f32 = 16.0;

use crate::{
    constants,
    components,
    components::{HasSpriteSize,HasTeam},
    resources
};

pub struct BuildColliderSystem;

fn build_shape_for_sprite<T: HasSpriteSize>(sprite: &T) -> shape::ShapeHandle<f32> {
    let (wpx, hpx) = sprite.sprite_size();
    let (wref, href) = (wpx / PIXELS_PER_COLLIDER_UNIT, hpx / PIXELS_PER_COLLIDER_UNIT);
    shape::ShapeHandle::new(shape::Cuboid::new(ncna::Vector2::new(
        wref / 2.0,
        href / 2.0
    )))
}

fn build_collision_groups_for_team<T: HasTeam>(teamed: &T) -> ncworld::CollisionGroups {
    let collision_group_id = match teamed.team() {
        components::Team::Allied | components::Team::Player => 1,
        components::Team::Enemy => 2
    };
    let mut collision_groups = ncworld::CollisionGroups::new();
    collision_groups.set_membership(&[collision_group_id]);
    collision_groups.set_blacklist(&[collision_group_id]);
    collision_groups
}

pub fn build_pos_for_transform(transform: &components::Transform) -> ncna::Isometry2<f32> {
    let translation = transform.translation();
    let (xref, yref) = (
        translation.x / PIXELS_PER_COLLIDER_UNIT, 
        translation.y / PIXELS_PER_COLLIDER_UNIT
    );

    ncna::Isometry2::new(
        ncna::Vector2::new(xref, yref),
        transform.rotation().euler_angles().2
    )
}

impl<'s> System<'s> for BuildColliderSystem {
    type SystemData = (
        WriteStorage<'s, components::Collider>,
        ReadStorage<'s, components::Character>,
        ReadStorage<'s, components::Attack>,
        ReadStorage<'s, components::Transform>,
        Write<'s, resources::CollisionDetection>,
        Entities<'s>
    );

    fn run(&mut self, (mut colliders, characters, attacks, transforms, mut collision_detection, entities): Self::SystemData) {
        let mut colliders_to_add = Vec::<(Entity, ncworld::CollisionObjectHandle)>::new();
        for (_, character, entity, transform) in (!&colliders, &characters, &entities, &transforms).join() {
            let shape = build_shape_for_sprite(character);
            let collision_groups = build_collision_groups_for_team(character);
            let pos = build_pos_for_transform(transform);

            let collision_object = collision_detection.world.add(
                pos, 
                shape, 
                collision_groups, 
                ncworld::GeometricQueryType::Proximity(constants::AGGRO_RANGE / PIXELS_PER_COLLIDER_UNIT), 
                entity
            );

            colliders_to_add.push((entity.clone(), collision_object.handle()));
        }

        for (_, attack, entity, transform) in (!&colliders, &attacks, &entities, &transforms).join() {
            let shape = build_shape_for_sprite(attack);
            let collision_groups = build_collision_groups_for_team(attack);
            let pos = build_pos_for_transform(transform);

            let collision_object = collision_detection.world.add(
                pos, 
                shape, 
                collision_groups, 
                ncworld::GeometricQueryType::Proximity(0.0), 
                entity
            );

            colliders_to_add.push((entity.clone(), collision_object.handle()));
        }

        for (entity, handle) in colliders_to_add {
            println!("registered collider for entity {}", entity.id());
            colliders.insert(
                entity, 
                components::Collider { handle }
            ).unwrap();
        }
    }
}