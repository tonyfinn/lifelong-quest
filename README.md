* Everything has a price

    - Spend life points to acquire stuff that increases your score at the end. But lowers your max HP (BoI has something like this?)
    - Hack and Slash - Where your character ages up every time:
        - he uses a powerful ability
        - buys something from a vendor?
        - too high and you die
        - Also affects physical stats
        - Find 4 artifacts on the map